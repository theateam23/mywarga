import { Component, Input, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

/**
 * Generated class for the SocMedBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'soc-med-bar',
  templateUrl: 'soc-med-bar.html'
})
export class SocMedBarComponent {
    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    content: Content;

  text: string;

  constructor() { }

  onEvent(event: string, item: any, e: any) {
    if (e) {
        e.stopPropagation();
    }
    if (this.events[event]) {
        this.events[event](item);
    }
}

}
