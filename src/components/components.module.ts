import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SocMedBarComponent } from './soc-med-bar/soc-med-bar';
import { IonicPageModule } from 'ionic-angular';
@NgModule({
	declarations: [SocMedBarComponent],
	imports: [IonicPageModule.forChild(SocMedBarComponent)],
	exports: [SocMedBarComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
