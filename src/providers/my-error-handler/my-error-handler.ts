import {Pro} from '@ionic/pro';
import {Injectable} from '@angular/core';
import {IonicErrorHandler} from 'ionic-angular';
import {LoggerService} from "../../services/LoggerService";

// initializing the Ionic Pro client
const IonicPro = Pro.init('aed40c9b', {
  appVersion: "1.5.1"
});
@Injectable()
export class MyErrorHandlerProvider extends IonicErrorHandler{
  constructor(private logger:LoggerService) {
    super();
  }
  handleError(err: any): void {
    super.handleError(err);
    IonicPro.monitoring.handleNewError(err); // Remove this if you want to disable Ionic's auto exception handling in development mode.
    this.logger.error(err);
  }
}
