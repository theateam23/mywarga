import {Injectable} from '@angular/core';
import 'rxjs/add/operator/share'
import {Api} from '../api/api';

@Injectable()
export class PropertyProviders {
  _properties=[];
  _propertySelected:any;
  constructor(public api: Api) { }



  getPropertyByEmail(email:any){
    let seq = this.api.post('getPropertiesByUser',{email:email}).share();
    /*seq.subscribe((res: any) => {
      if(res.isSuccess=='Y'){
        this._properties = res.property;
      }
    }, err => {
      console.error('ERROR', err);
    });*/

    return seq;
  }

  getFacilitiesByPropertyId(input:any){
    let seq = this.api.post('getFacilityByPropertyId',input).share();
    return seq;
  }

}
