import {Injectable} from '@angular/core';
import {Api} from "../api/api";
import * as CryptoJS from 'crypto-js';
import {IdmUserServices} from "../../services/idmUserService";
import {PropertyProviders} from "../items/items";
import {AppVersion} from "@ionic-native/app-version";

/*
  Generated class for the GeneralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeneralProvider {
  lang:any="";
  dateTime:any;
  appName:any;
  constructor(public api: Api, public property: PropertyProviders, private imUser:IdmUserServices, private appVersion: AppVersion) {
    console.log('Hello GeneralProvider Provider');
    this.appVersion.getPackageName().then(appName=>{
      this.appName = appName;
    });
  }

  callBackend(endPoint:string,input:any){
    // this.theProvider.updateMessageCount();
    let propertySelected = this.property._propertySelected;
    let propertyId = "";
    if(propertySelected==undefined||propertySelected===null){
      propertyId="";
    } else {
      propertyId = propertySelected.id;
    }
    this.dateTime = new Date().getTime();
    let data = { ...input, apiKey: this.generateApiKey(),apiTime:this.generateApiTime(), apiEmail:this.imUser.emailUser, apiProperty:propertyId, dateTime:this.dateTime,appName:this.appName };
    return this.api.post(endPoint, data).share();
  }

  generateApiKey(){
    let email = this.imUser.emailUser;
    let propertySelected = this.property._propertySelected;
    let propertyId = "";
    if(propertySelected==undefined||propertySelected===null){
      propertyId="";
    } else {
      propertyId = propertySelected.id;
    }
    let data = email.concat(propertyId).concat(this.dateTime.toString());
    let hash = CryptoJS.SHA256(data).toString();
    // console.log("hex -> "+CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex));
    // console.log("normal -> "+CryptoJS.SHA256(data).toString());
    return hash;
  }

  generateApiTime(){
    return this.dateTime - 1235724680;
  }

}
