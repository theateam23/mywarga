import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ActionSheetController, AlertController, LoadingController, ToastController} from "ionic-angular";

/*
  Generated class for the UtilitiesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilitiesProvider {
  loading : any;
  constructor(public http: HttpClient,public loadingCtrl: LoadingController, public toastCtrl: ToastController, public alertCtrl:AlertController,public actionSheetCtrl:ActionSheetController) {
    // console.log('Hello UtilitiesProvider Provider');
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Silahkan Tunggu...',
      duration: 15000,
      spinner:"circles"
    });
    this.loading.present();
  }

  dismissLoading(){
    if(this.loading!=undefined)
      this.loading.dismiss();
  }

  loadToast(msg : any){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  loadAlert(title:any, content:any){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: content,
      buttons: ['OK']
    });
    alert.present();
  }

  openActionSheet(data) {
    let actionSheet = this.actionSheetCtrl.create({
      title: data.title,
      buttons: data.listBtn
    });
    actionSheet.present();
  }

}
