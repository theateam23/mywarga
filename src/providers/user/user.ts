import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share'
import {Injectable} from '@angular/core';

import {Api} from '../api/api';

@Injectable()
export class User {
  _user: any;

  constructor(public api: Api) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('login', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getUser(accountInfo:any){
    let seq = this.api.post('getUser', accountInfo).share();
    seq.subscribe((res: any) => {
      if (res.isSuccess == 'Y') {
        console.log("user found");
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  updateUser(accountInfo:any){
    let seq = this.api.post('updateUser', accountInfo).share();
    return seq;
  }

  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }
}
