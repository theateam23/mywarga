import {Injectable} from '@angular/core';
import {Api} from "../api/api";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share'

@Injectable()
export class VisitorProvider {
  _visitors: any;
  constructor(public api: Api) {
    console.log('Hello VisitorProvider Provider');
  }

  createVisitor(input:any){
    let seq = this.api.post('createVisitor', input).share();
    return seq;
  }

  getListVisitor(input:any){
    let seq = this.api.post('getVisitorList',input).share();
    seq.subscribe((res: any) => {
      if(res.isSuccess=='Y'){
        this._visitors = res.visitors;
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  updateVisitor(input:any){
    let seq = this.api.post('updateVisitorStatus',input).share();
    return seq;
  }

}
