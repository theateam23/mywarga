import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ListItem} from "./list-item";

@NgModule({
    declarations: [
        ListItem,
    ],
    imports: [
        IonicPageModule.forChild(ListItem),
    ],
    exports: [
      ListItem
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ListItemModule { }
