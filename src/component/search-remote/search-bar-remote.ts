import {Component, Input} from '@angular/core';
import {IonicPage} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'search-bar-remote',
    templateUrl: 'search-bar-remote.html'
})
export class SearchBarRemote {
  @Input() data: any;
  @Input() events: any;

  searchTerm:any="";
  allItems:any;
  isDisplay:any;

  constructor() {}

  onResetForm(){
    this.isDisplay = false;
    this.searchTerm="";
  }

  onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
    if (this.events[event]) {
        this.events[event](item);
        this.isDisplay = true;
    }
    console.log(event);
  }
}
