import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchBarRemote} from './search-bar-remote';

@NgModule({
    declarations: [
        SearchBarRemote,
    ],
    imports: [
        IonicPageModule.forChild(SearchBarRemote),
    ],
    exports: [
        SearchBarRemote
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SearchBarRemoteModule { }
