import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchMemberPage} from './search-member';
import {SearchBarRemoteModule} from "../../component/search-remote/search-bar-remote.module";

@NgModule({
  declarations: [
    SearchMemberPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchMemberPage),
    SearchBarRemoteModule
  ],
})
export class SearchMemberPageModule {}
