import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ListPropertyPage} from './list-property';

@NgModule({
  declarations: [
    ListPropertyPage,
  ],
  imports: [
    IonicPageModule.forChild(ListPropertyPage),
  ],
})
export class ListPropertyPageModule {}
