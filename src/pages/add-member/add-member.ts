import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IdmUserServices} from "../../services/idmUserService";
import {GeneralProvider} from "../../providers/general/general";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {LIST_GENDER, LIST_STATUS_MEMBER} from "../../globalConstant/constant";

/**
 * Generated class for the AddMemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-member',
  templateUrl: 'add-member.html',
})
export class AddMemberPage {
  memberForm:FormGroup;
  listRelation:any=[];
  title:any;
  base64Image:any;
  strbase64:any;
  listGender:any;
  listStatus:any;

  constructor(public navCtrl: NavController,public imUser:IdmUserServices, public formBuilder: FormBuilder,
              public camera:Camera, public navParams: NavParams,public viewCtrl: ViewController, public general:GeneralProvider,
              private util:UtilitiesProvider) {
    this.memberForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      dateOfBirth: ['', Validators.compose([Validators.required])],
      placeOfBirth: ['', Validators.compose([Validators.required])],
      // relationship: ['', Validators.compose([Validators.required])],
      profession: [''],
      // idKTP: [''],
      email: ['', Validators.compose([Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")])],
      // address: [''],
      gender: ['', Validators.compose([Validators.required])]
    });

    this.base64Image = "";
    this.strbase64 = "";
    // this.listRelation.push({id:"Suami",name:"Suami"},{id:"Istri",name:"Istri"},{id:"Anak",name:"Anak"},{id:"Orang Tua",name:"Orang Tua"},{id:"ART",name:"Asisten Rumah Tangga"},{id:"Lainnya",name:"Lainnya"});
    // this.listRelation = LIST_RELATION;
    this.listGender = LIST_GENDER;
    this.listStatus = LIST_STATUS_MEMBER;
  }

  ionViewDidEnter() {
    this.title = "Tambah";
    if(this.navParams.get( 'memberId' )!==""&& this.navParams.get( 'memberId' )!== undefined){
      this.title = "Ubah";
      this.general.callBackend("getMember",{memberId:this.navParams.get( 'memberId' )}).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.memberForm.controls['name'].setValue(data.member.name);
          this.memberForm.controls['dateOfBirth'].setValue(data.member.dateOfBirth);
          this.memberForm.controls['placeOfBirth'].setValue(data.member.placeOfBirth);
          // this.memberForm.controls['relationship'].setValue(data.member.relationship);
          this.memberForm.controls['profession'].setValue(data.member.profession);
          // this.memberForm.controls['idKTP'].setValue(data.member.idKTP);
          this.memberForm.controls['email'].setValue(data.member.email);
          // this.memberForm.controls['address'].setValue(data.member.address);
          this.memberForm.controls['gender'].setValue(data.member.gender);
          this.base64Image = data.member.imageUrl==""?"":data.member.imageUrl;
        }
      });
    } else {
      this.base64Image = "assets/img/people.png";
    }

  }

  dismiss(send) {
    if(send==='Y'){
      if(this.memberForm.valid){
        this.memberForm.value.picture = this.strbase64;
        this.viewCtrl.dismiss(this.memberForm.value);
      }
    } else {
      this.viewCtrl.dismiss(send);
    }
  }

  takePicture(){
    let options = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 512,
      targetHeight: 512,
      quality:75,
      correctOrientation: true,
      allowEdit:true
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  openGallery(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality:50,
      targetWidth: 512,
      targetHeight: 512,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
      // console.log(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  takeOrUploadPicture(){
    let dataInput= {
      title : "Please Choose",
      listBtn : [
        {
          text: "Open Gallery",
          handler:(): void => {this.openGallery()}
        },
        {
          text: "Take Picture",
          handler:(): void => {this.takePicture()}
        }
      ]
    };
    this.util.openActionSheet(dataInput);
  }
}
