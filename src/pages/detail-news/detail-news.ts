import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-detail-news',
  templateUrl: 'detail-news.html',
})
export class DetailNewsPage {
  content:any;
  params:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing) {
    this.content = navParams.get('content');

    this.params.events = {
      'onShare': (item: any): void => {this.onShare()},
      'onWaShare': (item: any): void => {this.onWaShare()},
      'onFbShare': (item: any): void => {this.onFbShare()},
      'onTwShare': (item: any): void => {this.onTwShare()},
      
    };

    this.params.data = {
      'share' : "",
      'fb' : "",
      'wa' : "",
      'tw' : ""
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DetailNewsPage'+ JSON.stringify(this.navParams.get('content')));
  }

  compilemsg():string{
    var msg = this.content;
    return msg.concat(" \n Dikirim dari aplikasi Warga Kita !");
  }

  onShare(){
    var msg = this.compilemsg();
      this.socialSharing.share(msg, null, null, null);
  }

  onWaShare(){
    var msg = this.compilemsg();
      this.socialSharing.shareViaWhatsApp(msg, null, null);
  }

  onFbShare(){
    var msg = this.compilemsg();
      this.socialSharing.shareViaTwitter(msg, null, null);
  }

  onTwShare(){
    var msg = this.compilemsg();
      this.socialSharing.shareViaTwitter(msg, null, null);
  }
}
