import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetailNewsPage} from './detail-news';
import {DirectivesModule} from "../../directives/directives.module";
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DetailNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailNewsPage),
    DirectivesModule,
    ComponentsModule
  ],
})
export class DetailNewsPageModule {}
