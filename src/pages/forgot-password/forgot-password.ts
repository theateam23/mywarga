import {Component} from '@angular/core';
import {IonicPage, NavController, ViewController} from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  user: { email: string, password: string } = {
    email: '',
    password: ''
  };
  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {
  }

  dismiss(send) {
    if(send==='Y'){
      this.viewCtrl.dismiss(this.user.email);
    } else {
      this.viewCtrl.dismiss(send);
    }
 }

}
