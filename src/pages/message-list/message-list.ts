import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {NativeStorage} from "@ionic-native/native-storage";
import {GeneralProvider} from "../../providers/general/general";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {IdmUserServices} from "../../services/idmUserService";
import {theProvider} from "../../services/theProvider";

/**
 * Generated class for the MessageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message-list',
  templateUrl: 'message-list.html',
})
export class MessageListPage {

  noMessages:boolean;
  listMessage:any[];
  listMessageId:any[];
  propertyId:string;
  emailUser:string;
  params:any={};
  isSecurity:boolean;
  isEdit:boolean;
  isSearch:boolean;

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              public util:UtilitiesProvider,
              public nativeStorage:NativeStorage,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              private imUser:IdmUserServices,
              public theProvider:theProvider,
              public general:GeneralProvider) {

    this.noMessages = false;
    this.emailUser = this.imUser.emailUser;

    this.listMessage = [];

    /*this.events.subscribe('updateScreenMessage', () => {
      this.zone.run(() => {
        console.log('force update the screen');
        this.getData();
      });
    });*/

    if(this.isSecurity){
      this.isEdit = true;
      this.isSearch = false;
    } else{
      this.isEdit = false;
      this.isSearch = false;
    }

    this.params.data = {
      "title": "Tidak ada Pesan",
      "iconNoItem": "icon-alert-box",
      "noItem": this.noMessages,
      "isEdit": false,
      "isAdd": false,
      "isSearch": false,
      "items": this.listMessage,
    };

    this.params.events = {
      'onDelete': (item: any): void => {this.deleteMessage(item)},
      'onItemClick': (item: any): void => {this.detailMessage(item)},
      'onTextChange': function(text: any) {
        console.log("onTextChange");
      }
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageListPage');
  }

  getData(){
    let input = {
      email: this.emailUser
    };
    this.general.callBackend('getMessagesByUser',input).subscribe((data:any) => {
        console.log(data);
        // this.util.dismissLoading();
        if(data.isSuccess=='Y'){
          if(data.messages.length>0){
            this.listMessage = [];
            this.listMessageId = [];
            for (let i=0; i<data.messages.length;i++) {
              let messageMap:any={};
              let message = data.messages[i];
              messageMap.title = message.title;
              messageMap.id=message.id;
              messageMap.content = message.content;
              messageMap.isRead = message.isRead;
              messageMap.dateTime = message.dateTime;
              messageMap.image = message.isRead=='Y'?"assets/img/mailopen.png":"assets/img/mailclose.png";
              messageMap.iconDelete="icon-delete";
              messageMap.iconUndo="icon-undo-variant";
              this.listMessage.push(messageMap);
              this.listMessageId.push(message.id)
            }
            this.noMessages=false;
          } else {
            this.noMessages=true;
          }
          this.params.data = {
            "title": "Tidak ada Pesan",
            "iconNoItem": "icon-alert-box",
            "noItem": this.noMessages,
            "isEdit": false,
            "isAdd": false,
            "isSearch": false,
            "items": this.listMessage,
          };
          this.theProvider.updateMessageCount();
        } else {
          this.util.loadAlert("Error", data.message);
        }
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

  ionViewWillEnter() {
    this.util.presentLoading();
    this.getData();
    this.util.dismissLoading();
  }

  deleteMessage(item){
    this.util.presentLoading();
    let input = {
      email:this.emailUser,
      messageId:item
    };
    this.general.callBackend("deleteMessageById",input).subscribe((data:any)=>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.noMessages=false;
        this.getData();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });
  }

  deleteAllMessage(){
    this.util.presentLoading();
    let input = {
      email:this.emailUser,
      messageIdList:this.listMessageId
    };
    this.general.callBackend("deleteMessageByListId",input).subscribe((data:any)=>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.noMessages=false;
        this.getData();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses 💪", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });
  }

  detailMessage(item){
    let modal = this.modalCtrl.create('DetailMesagePage',{message: item});
    modal.present();
    modal.onDidDismiss(data=>{
        this.util.presentLoading();
        this.general.callBackend("readMessageById",{"messageId":item.id}).subscribe((data:any)=>{
          console.log(data);
          this.zone.run(() => {
            console.log('force update the screen');
            this.getData();
            this.util.dismissLoading();
          });
        });
    });
  }

}
