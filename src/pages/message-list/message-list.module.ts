import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MessageListPage} from './message-list';
import {ListItemModule} from "../../component/listingView/list-item.module";

@NgModule({
  declarations: [
    MessageListPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageListPage),
    ListItemModule
  ],
})
export class MessageListPageModule {}
