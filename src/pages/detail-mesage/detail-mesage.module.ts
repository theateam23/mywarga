import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetailMesagePage} from './detail-mesage';

@NgModule({
  declarations: [
    DetailMesagePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMesagePage),
  ],
})
export class DetailMesagePageModule {}
