import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the DetailMesagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-mesage',
  templateUrl: 'detail-mesage.html',
})
export class DetailMesagePage {

  messageDetail:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    this.messageDetail = navParams.get('message');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailMesagePage');
  }

  dismiss() {
      this.viewCtrl.dismiss();
  }

}
