import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {HomeGuardListPage} from './home-guard-list';
import {ListItemModule} from "../../component/listingView/list-item.module";

@NgModule({
  declarations: [
    HomeGuardListPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeGuardListPage),
    ListItemModule
  ],
})
export class HomeGuardListPageModule {}
