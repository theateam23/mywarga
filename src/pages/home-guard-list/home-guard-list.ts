import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {GeneralProvider} from "../../providers/general/general";
import {NativeStorage} from "@ionic-native/native-storage";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {IdmUserServices} from "../../services/idmUserService";
import {PropertyProviders} from "../../providers/items/items";
import {Camera} from "@ionic-native/camera";

/**
 * Generated class for the HomeGuardListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-guard-list',
  templateUrl: 'home-guard-list.html',
})
export class HomeGuardListPage {

  noJagain:boolean;
  listJagain:any[];
  propertyId:string;
  emailUser:string;
  params:any={};
  isSecurity:any;
  isEdit:boolean;
  isSearch:boolean;
  isAdd:boolean;

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              private camera:Camera,
              public util:UtilitiesProvider,
              public nativeStorage:NativeStorage,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              private property:PropertyProviders,
              public general:GeneralProvider,public imUser:IdmUserServices) {
    this.noJagain = false;
    this.emailUser = this.imUser.emailUser;
    this.isSecurity = this.imUser.getRole();
    this.propertyId = this.property._propertySelected.id;
    this.listJagain = [];

    /*this.events.subscribe('updateScreenJagain', () => {
      this.zone.run(() => {
        console.log('force update the screen');
        this.getListJagain();
      });
    });*/

    if(this.isSecurity){
      this.isEdit = false;
      this.isSearch = true;
      this.isAdd = false;
    } else{
      this.isEdit = false;
      this.isSearch = false;
      this.isAdd = true;
    }

    this.params.data = {
      "title": "Tidak ada data",
      "iconNoItem": "icon-alert-octagon",
      "noItem": this.noJagain,
      "isEdit": this.isEdit,
      "isAdd": this.isAdd,
      "isSearch": this.isSearch,
      "items": this.listJagain
    };

    this.params.events = {
      'onItemClick': function (item: any) {
        console.log(item);
      },
      'onDelete': (item: any): void => {
        if(this.isSecurity){
          console.log("on check in");
          this.updateStatus(item);
        } else{
          this.deleteJagain(item)
        }
      },
      'onEdit': (item: any): void => {},
      'onFab': (item: any): void => {this.presentModal()},
      'onTextChange': function(text: any) {
        console.log("onTextChange");
      }
    };

  }

  ionViewWillEnter() {
    this.util.presentLoading();
    this.getListJagain();
    if(this.isSecurity) {
      let elms = document.querySelectorAll(".list-content");
      for(let i=0;i<elms.length;i++) {
        let elm = <HTMLElement>elms[i];
        elm.style.marginTop = "7vh";
      }
    }
    this.util.dismissLoading();
  }

  presentModal(item?: any) {
    let modal = this.modalCtrl.create('AddHomeGuardPage',{vehicleId: item});
    modal.present();
    modal.onDidDismiss(data=>{
      console.log(data);
      if(data!=undefined&&data!='N'){
        this.util.presentLoading();
        var input=data;
        input.propertyId = this.propertyId;
        input.email = this.emailUser;
        this.general.callBackend("addHomeGuard",input).subscribe((data:any)=>{
          console.log(data);
          // this.util.dismissLoading();
          if(data.isSuccess=='Y'){
            this.noJagain=false;
            /*this.util.loadAlert("Sukses", data.message);
            this.events.publish('updateScreenJagain');*/
            this.getListJagain();
            this.zone.run(() => {
              console.log('force update the screen');
              this.util.dismissLoading();
              this.util.loadAlert("Sukses", data.message);
            });
          } else {
            this.util.loadAlert("Error", data.message);
          }
        });
      }
    });
  }

  getListJagain(){
    // this.util.presentLoading();
    let input = {
      email: this.emailUser,
      propertyId:this.propertyId
    };
    this.general.callBackend('getHomeGuardList',input).subscribe((data:any) => {
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listJagain=[];
          if(data.homeGuardList.length>0){
            for (let i=0; i<data.homeGuardList.length;i++) {
              let jagain = data.homeGuardList[i];
              let jagainMap:any={};
              jagainMap.title = jagain.fromDate+" - "+jagain.toDate;
              jagainMap.description = jagain.description ;
              if(this.isSecurity){
                jagainMap.title = jagain.unit;
                jagainMap.subTitle = jagain.subArea;
                jagainMap.description = jagain.area;
              }
              jagainMap.id = jagain.homeGuardId;
              jagainMap.image = "assets/img/jagain.png";
              jagainMap.iconDelete=this.isSecurity?"icon-check":"icon-delete";
              jagainMap.iconEdit="icon-check";
              jagainMap.iconUndo="icon-undo-variant";
              this.listJagain.push(jagainMap);
            }
            this.noJagain=false;
          } else {
            this.noJagain=true;
          }
          this.params.data = {
            "title": "Tidak ada data",
            "iconNoItem": "icon-alert-octagon",
            "noItem": this.noJagain,
            "isEdit": this.isEdit,
            "isAdd": this.isAdd,
            "isSearch": this.isSearch,
            "items": this.listJagain
          };
        } else {
          this.util.loadAlert("Error", data.message);
        }
        // this.util.dismissLoading();
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

  deleteJagain(item){
    this.util.presentLoading();
    let input = {
      email:this.emailUser,
      homeGuardId:item
    };
    this.general.callBackend("deleteHomeGuard",input).subscribe((data:any)=>{
      console.log(data);
      // this.util.dismissLoading();
      if(data.isSuccess=='Y'){
        this.noJagain=false;
        /*this.util.loadAlert("Sukses", data.message);
        this.events.publish('updateScreenJagain');*/
        this.getListJagain();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });

  }

  updateStatus(item){
    let options = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 512,
      targetHeight: 512,
      quality:75,
      correctOrientation: true,
      allowEdit:true
    };
    this.camera.getPicture(options).then((imageData) => {
      this.util.presentLoading();
      let input = {
        email:this.emailUser,
        imageFile:imageData,
        homeGuardId:item
      };
      this.general.callBackend("checkHomeGuard",input).subscribe((data:any)=>{
        console.log(data);
        // this.util.dismissLoading();
        if(data.isSuccess=='Y'){
          this.noJagain=false;
          /*this.util.loadAlert("Sukses", data.message);
          this.events.publish('updateScreenJagain');*/
          this.getListJagain();
          this.zone.run(() => {
            console.log('force update the screen');
            this.util.dismissLoading();
            this.util.loadAlert("Sukses", data.message);
          });
        } else {
          this.util.loadAlert("Error", data.message);
        }
      });
    }, (err) => {
      console.log(err);
    });

  }

}
