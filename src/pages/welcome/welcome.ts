import {Component} from '@angular/core';
import {
  Events,
  IonicPage,
  MenuController,
  ModalController,
  NavController,
  Platform,
  ToastController
} from 'ionic-angular';
import * as firebase from "firebase/app";
import {AngularFireAuth} from "angularfire2/auth";
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import {IdmUserServices} from "../../services/idmUserService";
import {User} from "../../providers/providers";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {PropertyProviders} from "../../providers/items/items";
import {NativeStorage} from "@ionic-native/native-storage";
import {MainPage, SignUpPage} from "../pages";
import {Firebase} from "@ionic-native/firebase";
import {GeneralProvider} from "../../providers/general/general";

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
  providers: [GooglePlus,Facebook]
})
export class WelcomePage {

  account: any = {};
  fbColor: String ='#161D46';
  twColor: String ='#161D46';
  gColor: String ='#161D46';

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public user: User,
              public toastCtrl: ToastController,
              private fAuth: AngularFireAuth,
              public nativeStorage: NativeStorage,
              private fb: Facebook,
              private googlePlus: GooglePlus,
              private imUserService: IdmUserServices,
              public util : UtilitiesProvider,
              public property: PropertyProviders,
              public platform: Platform,
              private menuCtrl: MenuController,
              public events: Events,
              private general:GeneralProvider,
              public fBase:Firebase) {
    this.menuCtrl.enable(false,'otherMenu');


  }

  presentModal(modalPage) {
    let modal = this.modalCtrl.create(modalPage);
    modal.present();
    modal.onDidDismiss(data=>{
      console.log("reset password ==> "+ data);
      if(data!="N"){
        if(this.isValidMailFormat(data)){
          return this.fAuth.auth.sendPasswordResetEmail(data).then(() => {
            console.log('sent Password Reset Email!');
            this.util.loadAlert("Sukses","Permintaan perubahan password sudah dikirim ke email anda, silahkan cek email anda.");
          } ) .catch((error) => {
            console.error(error);
            if(error.code.indexOf("auth/user-not-found") >= 0) {
              this.util.loadAlert("Info","Cek email anda untuk verifikasi, atau lakukan registrasi dulu");
            } else if(error.code.indexOf("auth/invalid-email")>=0){
              this.util.loadAlert("Sorry...:(","Format email yang anda masukkan salah");
            }
          });
        } else {
          this.util.loadAlert("Sorry...:(","Email yang anda masukkan salah format");
        }
      }
    });
  }

  isValidMailFormat(email: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if ((email.length === 0) && (!EMAIL_REGEXP.test(email))) {
      return false;
    }
    return true;
  }

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }


  async loginEmail() {
    this.fAuth.auth.signOut();
    try {
      var r = await this.fAuth.auth.signInWithEmailAndPassword(
        this.account.email,
        this.account.password
      );
      if (r) {
        this.imUserService.emailUser = firebase.auth().currentUser.email;
        this.nativeStorage.setItem("emailUser",firebase.auth().currentUser.email);
        this.checkUserExist();
      }
    } catch (err) {
      // this.util.loadToast(err.message);
      console.error(err.code);
      if(err.code.indexOf("wrong-password") >= 0) {
        this.util.loadToast("Invalid email or password");
      } else if(err.code.indexOf("user-not-found") >= 0){
        this.util.presentLoading();
        this.checkUserExist();
      }
    }
  }

  checkUserExist(){
    let currentUser = firebase.auth().currentUser;
    if(currentUser!=null||currentUser!=undefined){
      this.account.email = currentUser.email;
    }

    // this.user.getUser(this.account).subscribe((resp:any) => {
    this.general.callBackend("getUser",this.account).subscribe((resp:any) => {
        console.log("check user --->"+JSON.stringify(resp));
        this.util.dismissLoading();
        if(resp.isSuccess=="Y"){
          this.imUserService.setRole(resp.user.isSecurity=="Y"?true:false);
          this.imUserService.loginFlag=true;
          this.nativeStorage.setItem("isLogin",true);
          let paramSideMenu:any = {};
          paramSideMenu.isSecurity = resp.user.isSecurity;
          paramSideMenu.type = "PRM";

          this.events.publish("UPDATE_SIDE_MENU", paramSideMenu);

          //masuk halaman dashboard
          this.navCtrl.push(MainPage);
          console.log("prepare subscribe "+resp.user.id+" "+this.imUserService.emailUser);
          this.fBase.subscribe(resp.user.id).then(dataSubscribe=>{
            console.log("sukses subscribe topic private");
          }).catch(error=>{
            console.error(error);
            console.log("gagal subscribe private");
          });
        }else if("N"===resp.needRegistration){
          this.util.loadAlert("Information",resp.message);
        } else {
          this.imUserService.setEmail(this.account.email);
          this.navCtrl.push(SignUpPage);
        }

      },e => {
        this.util.dismissLoading();
        let toast = this.toastCtrl.create({
          message: "Server on maintenance, please retry again later.",
          duration: 3000,
          position: 'top'
        });
        toast.present();
        console.log(e)
      }
    );
  }

  async loginWithFacebook() {
    this.fAuth.auth.signOut();
    this.fbColor='#4EC2BE';
    this.gColor='#161D46';
    this.twColor = '#161D46';
    try{
      var result = await this.fb.login(['email']);
      // this.util.presentLoading();
      const fbCredential = firebase.auth.FacebookAuthProvider.credential(result.authResponse.accessToken);

      await firebase.auth().signInWithCredential(fbCredential).then(res => {
        this.imUserService.emailUser = firebase.auth().currentUser.email;
        this.nativeStorage.setItem("emailUser",firebase.auth().currentUser.email);
        this.checkUserExist();
      });
    }catch(err){
      this.util.dismissLoading();
      console.error("error di FB " + err.errorMessage);
      this.util.loadToast(err.errorMessage)
    }

  }

  registerUser(){
    this.navCtrl.push(SignUpPage);
  }

  loginWithGoogle() {
    this.fAuth.auth.signOut();
    this.gColor='#4EC2BE';
    this.twColor = '#161D46';
    this.fbColor = '#161D46';
    let webClientId = '916556318943-82jn1h2hs43h6tql9rhmk9en8ch3f715.apps.googleusercontent.com';
    if (this.platform.is('ios')) {
      webClientId = '916556318943-s374upnr1rk9imnjk7fqh7n4bodlu3k7.apps.googleusercontent.com';
    }
    this.googlePlus.login({
      'webClientId': webClientId,
      'offline': true
    }).then(res => {
      // this.util.presentLoading();
      const googleCredential = firebase.auth.GoogleAuthProvider.credential(res.idToken);

      firebase.auth().signInWithCredential(googleCredential).then( response => {
        this.imUserService.emailUser = firebase.auth().currentUser.email;
        this.nativeStorage.setItem("emailUser",firebase.auth().currentUser.email);
        this.checkUserExist();
      }).catch(err => {
        console.error(err);
        let toast = this.toastCtrl.create({
          message: err,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });
    }).catch(err => {
      // this.util.dismissLoading();
        console.error(err);
        var errorMsg = "";
        if(err=="12510"){
          errorMsg = "User Cancelled";
        } else {
          errorMsg = "Unknown Error, Please Contact Admin";
        }
        this.util.loadToast(errorMsg);
      });
  }
}
