import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetailVehiclePage} from './detail-vehicle';

@NgModule({
  declarations: [
    DetailVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailVehiclePage),
  ],
})
export class DetailVehiclePageModule {}
