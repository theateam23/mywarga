import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GeneralProvider} from "../../providers/general/general";

/**
 * Generated class for the DetailVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-vehicle',
  templateUrl: 'detail-vehicle.html',
})
export class DetailVehiclePage {

  vehicleForm:FormGroup;
  title:any;
  minDate:any;
  base64Image:any;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder,
              public navParams: NavParams,public viewCtrl: ViewController, private general:GeneralProvider) {

    this.vehicleForm = formBuilder.group({
      plateNo: ['', Validators.compose([Validators.required])],
      vehicleType: ['', Validators.compose([Validators.required])],
      color: ['', Validators.compose([Validators.required])],
      brand: [''],
      taxDueDate: [''],
    });
  }

  ionViewDidEnter() {
    this.general.callBackend("getVehicle",{vehicleId:this.navParams.get( 'vehicleId' )}).subscribe((data:any)=>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.vehicleForm.controls['plateNo'].setValue(data.vehicle.plateNo);
        this.vehicleForm.controls['vehicleType'].setValue(data.vehicle.vehicleType);
        this.vehicleForm.controls['color'].setValue(data.vehicle.color);
        this.vehicleForm.controls['brand'].setValue(data.vehicle.brand);
        this.vehicleForm.controls['taxDueDate'].setValue(data.vehicle.taxDueDate);
        this.base64Image = data.vehicle.imageUrl==""?"":data.vehicle.imageUrl;
      }
    });

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
