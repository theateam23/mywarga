import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, Platform} from 'ionic-angular';
import {NativeStorage} from "@ionic-native/native-storage";
import {Firebase} from "@ionic-native/firebase";
import {PropertyProviders} from "../../providers/items/items";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {ListingCategoryPage} from "../pages";
import {GeneralProvider} from "../../providers/general/general";
import {IdmUserServices} from "../../services/idmUserService";
import {theProvider} from "../../services/theProvider";


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  pageTitle:"";
  emailUser:String;
  propertyId:"";
  showDrop:boolean;
  showing:boolean;
  categoryList:any=[];
  newsList:any=[];
  isSecurity:any;

  constructor(public navCtrl: NavController,
              public property: PropertyProviders,
              public util : UtilitiesProvider,
              public modalCtrl: ModalController,
              private nativeStorage: NativeStorage,
              public events: Events,
              private zone: NgZone,
              public platform: Platform,
              public fBase:Firebase,
              private general:GeneralProvider,public imUser:IdmUserServices,
              public theProvider:theProvider) {
    this.isSecurity = this.imUser.getRole();
    this.emailUser = this.imUser.emailUser;
    this.property.getPropertyByEmail(this.emailUser).subscribe((data:any) => {
        console.log(data);
        if(data.isSuccess=='Y') {

          this.events.publish("UPDATE_IMG_SIDE_MENU", data.property.imageUrl);

          let paramSideMenu:any = {};
          paramSideMenu.isSecurity = this.isSecurity?"Y":"N";
          paramSideMenu.type = data.property.propertyType;
          this.events.publish("UPDATE_SIDE_MENU", paramSideMenu);

          this.property._properties = data.property;
          if (data.property.length > 1) {
            this.showDrop = true;
          } else {
            this.showDrop = false;
          }
          if (this.platform.is('ios')) {
            this.fBase.grantPermission();
          }
          console.log("prepare subscribe property => "+data.property.id);
          this.fBase.subscribe(data.property.id).then(dataSubscribe=>{
            console.log("sukses subscribe topic property");
          }).catch(error=>{
            console.error(error);
            console.log("gagal subscribe property");
          });
          if(!this.isSecurity){
            console.log("prepare subscribe area => "+data.property.areaId);
            this.fBase.subscribe(data.property.areaId).then(dataSubscribe=>{
              console.log("sukses subscribe topic area");
            }).catch(error=>{
              console.error(error);
              console.log("gagal subscribe area");
            });
            console.log("prepare subscribe penghuni => penghuni"+data.property.id);
            this.fBase.subscribe("security~"+data.property.id).then(dataSubscribe=>{
              console.log("sukses subscribe topic penghuni");
            }).catch(error=>{
              console.error(error);
              console.log("gagal subscribe penghuni");
            });
          } else {
            console.log("prepare subscribe security => security"+data.property.id);
            this.fBase.subscribe("security~"+data.property.id).then(dataSubscribe=>{
              console.log("sukses subscribe topic security");
            }).catch(error=>{
              console.error(error);
              console.log("gagal subscribe security");
            });
          }

          this.pageTitle = data.property.areaName;
          if(this.isSecurity)
            this.pageTitle = data.property.name;
          this.propertyId = data.property.id;
          this.property._propertySelected = data.property;
          this.theProvider.updateMessageCount();
        }
      },
      e => {
        console.log(e)
      }
    );
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });

    this.general.callBackend("getNewsCategory","").subscribe((response:any) => {
      if(response.isSuccess=='Y') {
          this.categoryList = response.categoryList;
      }
    });

  }

  detailCategory(categoryCode){
    let inputDetail = {
      email:this.emailUser,
      propertyId:this.propertyId,
      categoryCode:categoryCode
    };
    this.util.presentLoading();
    this.general.callBackend("getNewsByUserAndCategory",inputDetail).subscribe((response:any) => {
      if(response.isSuccess=='Y') {
        this.newsList = response.news;
        this.navCtrl.push(ListingCategoryPage,{news:this.newsList});
      } else {
        console.error(response.message);
      }
    });

  }

  ionViewWillEnter() {
    console.log("Dashboard ===> ionViewWillEnter");
    this.theProvider.updateMessageCount();
  }

  changeProperty(modalPage) {
    let modal = this.modalCtrl.create(modalPage, {propertyId: this.propertyId});
    modal.present();
    modal.onDidDismiss(data =>{
      if(data != undefined && data!=null){
        this.propertyId = data.id;
        this.nativeStorage.setItem("propertySelected",data);
        this.pageTitle = data.name;
        this.events.publish('updateScreen');
        /*console.log("prepare subsscribe news~"+data.id);
        this.fBase.subscribe("news~"+data.id).then(dataSubscribe=>{
          console.log("sukses subscribe topic news")
        }).catch(error=>{
          console.error(error);
          console.log("gagal subscribe news");
        });*/
      } else {
        console.log("cancel select property")
      }
    });
  }

  openModalPanic(){
    let modal = this.modalCtrl.create('PanicPage');
    modal.present();
    modal.onDidDismiss(data=>{
      console.log("close modal");
    });
  }

  callSOS(){
    navigator.vibrate(1000);
    //todo call SOS to backend
    this.nativeStorage.getItem('propertySelected')
      .then(
        dataStorage => {
          console.log(dataStorage);
          let input = {
            propertyId : dataStorage.id,
            email : this.imUser.emailUser
          };
          this.general.callBackend("pushNotification",input).subscribe((data:any)=>{
            if(data.isSuccess=='Y'){
              this.util.loadAlert("Thank You", "Security Will reach you ASAP");
              navigator.vibrate(500);
            } else {
              this.util.loadAlert("Sorry", "Sorry  we cannot reach security right now, Please contact manually");
            }
          });

        },
        error => {
          console.error(error);

        }
      );

  }

}
