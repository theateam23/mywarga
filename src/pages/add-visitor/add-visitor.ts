import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IdmUserServices} from "../../services/idmUserService";
import * as moment from "moment";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {GeneralProvider} from "../../providers/general/general";
import {PropertyProviders} from "../../providers/items/items";

/**
 * Generated class for the AddVisitorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-visitor',
  templateUrl: 'add-visitor.html',
})
export class AddVisitorPage {
  base64Image:string;
  strbase64:string;
  visitorForm:FormGroup;
  isSecurity:boolean;
  listBlock:any={};
  listUnit:any={};
  listArea:any={};
  propertyId:any="";
  minDate:any;
  constructor(public navCtrl: NavController,
              private general:GeneralProvider,
              public imUser:IdmUserServices,
              private util:UtilitiesProvider,
              public formBuilder: FormBuilder,
              public camera:Camera, public navParams: NavParams,
              private property:PropertyProviders,
              public viewCtrl: ViewController) {
    this.base64Image = "assets/img/people.png";
    this.strbase64 = "";
    this.isSecurity = this.imUser.getRole();
    this.propertyId = this.property._propertySelected.id;
    let now = moment('00:00','HH:mm');
    this.minDate = new Date().toISOString();
    this.visitorForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      timeVisit: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      dateVisit: ['', Validators.compose([Validators.required])],
      vehicleNo: ['', Validators.compose([])],
      phoneNo: [''],
    });

    if(this.isSecurity){
      this.visitorForm = formBuilder.group({
        name: ['', Validators.compose([Validators.required])],
        vehicleNo: ['', Validators.compose([])],
        area: ['', Validators.compose([])],
        block: ['', Validators.compose([])],
        subArea: ['', Validators.compose([])],
        unitId: ['', Validators.compose([])],
        phoneNo: ['', Validators.compose([Validators.required])],
      });
    }

    this.listUnit = [];
    this.listBlock = [];
    this.listArea = [];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddVisitorPage');
    this.getArea();
  }

  dismiss(send) {
      if(send==='Y'){
        if(this.visitorForm.valid){
          this.visitorForm.value.base64 = this.strbase64;
          this.viewCtrl.dismiss(this.visitorForm.value);
        }
      } else {
        this.viewCtrl.dismiss(send);
      }
  }

  takePicture(){
    let options = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 512,
      targetHeight: 512,
      quality:75,
      correctOrientation: true,
      allowEdit:true
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  openGallery(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality:75,
      targetWidth: 512,
      targetHeight: 512,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
      // console.log(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  takeOrUploadPicture(){
    let dataInput= {
      title : "Please Choose",
      listBtn : [
        {
          text: "Open Gallery",
          handler:(): void => {this.openGallery()}
        },
        {
          text: "Take Picture",
          handler:(): void => {this.takePicture()}
        }
      ]
    };
    this.util.openActionSheet(dataInput);
  }

  getArea(){
    let input = {propertyId:this.propertyId};

    this.general.callBackend("getAreasByProperty",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listArea = data.areaList;
        }
      },
      e => {
        console.log(e)
      });
  }

  getBlock(){
    let input = {areaId:this.visitorForm.value.area};

    this.general.callBackend("getSubAreaByAreaId",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listBlock = data.subAreaList;
        }
      },
      e => {
        console.log(e)
      });
  }

  getUnit(){
    let input = {propertyId:this.propertyId,subArea:this.visitorForm.value.block};
    this.general.callBackend("getUnitByPropertyAndSubArea",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listUnit = data.unitList;
        }
      },
      e => {
        console.log(e)
      });
  }

}
