import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GeneralProvider} from "../../providers/general/general";
import {Camera} from "@ionic-native/camera";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {LIST_VEHICLE_TYPE} from "../../globalConstant/constant";

/**
 * Generated class for the AddVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-vehicle',
  templateUrl: 'add-vehicle.html',
})
export class AddVehiclePage {

  vehicleForm:FormGroup;
  listVehicleType:any=[];
  title:any;
  minDate:any;
  base64Image:any;
  strbase64:any;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private util:UtilitiesProvider, public camera:Camera,
              public navParams: NavParams,public viewCtrl: ViewController, private general:GeneralProvider) {

    this.vehicleForm = formBuilder.group({
      plateNo: ['', Validators.compose([Validators.required])],
      vehicleType: ['', Validators.compose([Validators.required])],
      color: ['', Validators.compose([Validators.required])],
      brand: [''],
      taxDueDate: [''],
    });
    this.minDate = new Date().toISOString();
    this.listVehicleType.push({id:"Sepeda",name:"Sepeda"},{id:"Sepeda Motor",name:"Sepeda Motor"},{id:"Mobil",name:"Mobil"},{id:"Truk",name:"Truk"},{id:"Lainnya",name:"Lainnya"});
    this.listVehicleType = LIST_VEHICLE_TYPE;
    this.strbase64 = "";
  }

  ionViewDidEnter() {
    this.title = "Tambah";
    if(this.navParams.get( 'vehicleId' )!==""&& this.navParams.get( 'vehicleId' )!== undefined){
      this.title = "Ubah";
      this.general.callBackend("getVehicle",{vehicleId:this.navParams.get( 'vehicleId' )}).subscribe((data:any)=>{
        // console.log(data);
        if(data.isSuccess=='Y'){
          this.vehicleForm.controls['plateNo'].setValue(data.vehicle.plateNo);
          this.vehicleForm.controls['vehicleType'].setValue(data.vehicle.vehicleType);
          this.vehicleForm.controls['color'].setValue(data.vehicle.color);
          this.vehicleForm.controls['brand'].setValue(data.vehicle.brand);
          this.vehicleForm.controls['taxDueDate'].setValue(data.vehicle.taxDueDate);
          this.base64Image = data.vehicle.imageUrl==""?"":data.vehicle.imageUrl;
        }
      });
    } else{
      this.base64Image = "assets/img/cycle.png";
    }

  }

  dismiss(send) {
    if(send==='Y'){
      if(this.vehicleForm.valid){
        this.vehicleForm.value.picture = this.strbase64;
        this.viewCtrl.dismiss(this.vehicleForm.value);
      }
    } else {
      this.viewCtrl.dismiss(send);
    }
  }

  takePicture(){
    let options = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 512,
      targetHeight: 512,
      quality:75,
      correctOrientation: true,
      allowEdit:true
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  openGallery(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality:75,
      targetWidth: 512,
      targetHeight: 512,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
      // console.log(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  takeOrUploadPicture(){
    let dataInput= {
      title : "Please Choose",
      listBtn : [
        {
          text: "Open Gallery",
          handler:(): void => {this.openGallery()}
        },
        {
          text: "Take Picture",
          handler:(): void => {this.takePicture()}
        }
      ]
    };
    this.util.openActionSheet(dataInput);
  }
}
