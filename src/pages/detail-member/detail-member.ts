import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IdmUserServices} from "../../services/idmUserService";
import {GeneralProvider} from "../../providers/general/general";

/**
 * Generated class for the DetailMemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-member',
  templateUrl: 'detail-member.html',
})
export class DetailMemberPage {

  memberForm:FormGroup;
  title:any;
  base64Image:any;

  constructor(public navCtrl: NavController,public imUser:IdmUserServices, public formBuilder: FormBuilder,
              public navParams: NavParams,public viewCtrl: ViewController, public general:GeneralProvider) {
    this.memberForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      dateOfBirth: ['', Validators.compose([Validators.required])],
      placeOfBirth: ['', Validators.compose([Validators.required])],
      profession: [''],
      email: ['', Validators.compose([Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")])],
      gender: ['', Validators.compose([Validators.required])]
    });

    this.base64Image = "";
  }

  ionViewDidEnter() {
    this.general.callBackend("getMember",{memberId:this.navParams.get( 'memberId' )}).subscribe((data:any)=>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.memberForm.controls['name'].setValue(data.member.name);
        this.memberForm.controls['dateOfBirth'].setValue(data.member.dateOfBirth);
        this.memberForm.controls['placeOfBirth'].setValue(data.member.placeOfBirth);
        this.memberForm.controls['profession'].setValue(data.member.profession);
        this.memberForm.controls['email'].setValue(data.member.email);
        this.memberForm.controls['gender'].setValue(data.member.gender);
        this.base64Image = data.member.imageUrl==""?"assets/img/people.png":data.member.imageUrl;
      }
    });
  }
  dismiss() {
      this.viewCtrl.dismiss();
  }
}
