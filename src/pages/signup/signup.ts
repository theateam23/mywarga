import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {User} from '../../providers/providers';
import {FirstRunPage} from '../pages';
import {PropertyProviders} from "../../providers/items/items";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import * as firebase from "firebase/app";
import {GeneralProvider} from "../../providers/general/general";
import {LIST_STATUS_MEMBER} from "../../globalConstant/constant";
import {AngularFireAuth} from "angularfire2/auth";
import {IdmUserServices} from "../../services/idmUserService";

interface PropertyItem {
  id: string
  name: any
}
type ListProperty = PropertyItem[];

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignupPage {
  account: any = {};
  properties : ListProperty;
  selectedCheckBox = [];
  selectOptions:any;
  selectStatus:any;
  propertyId:any;
  isEmail:any;
  listStatus:any=[];
  listBlock:any={};
  listUnit:any={};
  listArea:any={};
  selectedProperty:any;
  selectedArea:any;
  selectedBlock:any;
  selectedUnit:any;
  selectedStatus:any;

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
              public user: User,
              public toastCtrl: ToastController,
              public property: PropertyProviders,
              public util:UtilitiesProvider,
              private fAuth: AngularFireAuth,
              private imUserService: IdmUserServices,
              private general:GeneralProvider) {


    this.selectedStatus="Pemilik";
    this.account.status = "Pemilik";
    this.selectStatus = {
      subTitle: 'Select your Status',
      mode: 'ios'
    };

    this.selectOptions = {
      title: 'List Property',
      subTitle: 'Select your property',
      mode: 'ios'
    };

    this.listStatus = LIST_STATUS_MEMBER;

    this.general.callBackend("getProperties","").subscribe((data:any) => {
        this.util.dismissLoading();
        console.log(data);
        if(data.isSuccess=='Y'){
          this.properties = data.property;
        }
      },
      e => {
        console.log(e)
      }
    );
    this.isEmail = true;
    let providerData = firebase.auth().currentUser!=null?firebase.auth().currentUser.providerData[0]:undefined;
    if(providerData!==undefined){
      this.account = providerData;
      this.account.name = providerData.displayName;
      this.account.mobileNo = providerData.phoneNumber;
      this.isEmail = providerData.providerId=="password"?true:false;
    } else {
      this.account.email = this.imUserService.getEmail();
    }

    this.listUnit = [];
    this.listBlock = [];
    this.listArea = [];
  }

  getArea(){
    let input = {propertyId:this.account.property};

    this.general.callBackend("getAreasByProperty",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listArea = data.areaList;
          if(this.listArea.length == 1){
            this.selectedArea=this.listArea[0].name;
            this.account.area=this.listArea[0].id;
            this.getBlock();
          }

        }
      },
      e => {
        console.log(e)
      });
  }

  getBlock(){
    let input = {areaId:this.account.area};

    this.general.callBackend("getSubAreaByAreaId",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listBlock = data.subAreaList;
          if(this.listBlock.length == 1){
            this.selectedBlock=this.listBlock[0].name;
            this.account.block=this.listBlock[0].id;
            this.getUnit();
          }

        }
      },
      e => {
        console.log(e)
      });
  }

  getUnit(){
    console.log(this.account.property);

    let input = {propertyId:this.account.property,subArea:this.account.block};

    this.general.callBackend("getUnitByPropertyAndSubArea",input).subscribe((data:any)=>{
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listUnit = data.unitList;
          if(this.listUnit.length == 1){
            this.selectedUnit=this.listUnit[0].name;
          }
        }
      },
      e => {
        console.log(e)
      });
  }

  getCheckBox(propId:any,isChecked){
    // console.log(propId);
    if(isChecked.checked) {
      this.selectedCheckBox.push(propId);
    } else {
      let idx = this.selectedCheckBox.indexOf(propId);
      this.selectedCheckBox.splice(idx,1);
    }
  }

  async register(accountInfo:any) {
    try {
      // console.log(accountInfo);
      const result = await this.fAuth.auth.createUserWithEmailAndPassword(
        accountInfo.email,
        accountInfo.password
      );
      if (result) {
        console.log(result);
      }
    } catch (e) {
      // this.util.dismissLoading();
      this.util.loadToast(e.message);
      console.error(e.message);
    }
  }

  doSignup() {
    this.account.propertyId = this.account.property;
    this.util.presentLoading();
      this.general.callBackend("registerUserMobile",this.account).subscribe((resp:any) => {
        if(resp.isSuccess=="Y"){
          this.util.loadToast(resp.message);
          this.navCtrl.push(FirstRunPage);
          this.register(this.account);
        } else if(resp.isSuccess=="N" && resp.status==99){
          this.util.loadToast(resp.message);
          this.navCtrl.push(FirstRunPage);
        } else {
          this.util.loadAlert("Error",resp.message);
        }
        this.util.dismissLoading();
      }, (err) => {
        let toast = this.toastCtrl.create({
          message: this.signupErrorString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });
  }
}
