import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DetailNewsPage} from "../pages";
import {UtilitiesProvider} from "../../providers/utilities/utilities";

/**
 * Generated class for the DetailCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-category',
  templateUrl: 'detail-category.html',
})
export class DetailCategoryPage {
  params:any={};
  newsList:any=[];
  constructor(public navCtrl: NavController,public util : UtilitiesProvider, public navParams: NavParams) {

    this.newsList = navParams.get("news");

    this.params.data = {
      "displayFab":false,
      "items":this.newsList,

    };

    this.params.events = {
      'onItemClick': (item: any): void => {this.onDetail(item)},
      'onDetail': (item: any): void => {this.onDetail(item)},
      'onTextChange': function(text: any) {
        console.log("onTextChange");
      }
    };
  }

  ionViewDidLoad() {
    this.util.dismissLoading();
    console.log('ionViewDidLoad DetailCategoryPage');
  }


  onDetail(item){
    this.navCtrl.push(DetailNewsPage,{
      content: item
    });
  }


}
