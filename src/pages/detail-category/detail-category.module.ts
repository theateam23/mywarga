import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetailCategoryPage} from './detail-category';
import {GoogleCardLayout2Module} from "../../component/google-layout-2/google-card-layout-2.module";
import {GoogleCardLayout1Module} from "../../component/google-layout-1/google-card-layout-1.module";

@NgModule({
  declarations: [
    DetailCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailCategoryPage),
    GoogleCardLayout2Module,
    GoogleCardLayout1Module
  ],
})
export class DetailCategoryPageModule {}
