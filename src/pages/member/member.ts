import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {NativeStorage} from "@ionic-native/native-storage";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {GeneralProvider} from "../../providers/general/general";
import {IdmUserServices} from "../../services/idmUserService";
import {theProvider} from "../../services/theProvider";

/**
 * Generated class for the MemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-member',
  templateUrl: 'member.html',
})
export class MemberPage {
  noMember:boolean;
  listMember:any[];
  propertyId:string;
  emailUser:string;
  isSecurity:boolean;
  isEdit:boolean;
  isSearch:boolean;
  profilePic:string;
  params:any={};

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              public util:UtilitiesProvider,
              public nativeStorage:NativeStorage,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public general:GeneralProvider,
              public theProvider:theProvider,
              private imUser:IdmUserServices) {
    this.noMember = false;
    this.emailUser = this.imUser.emailUser;
    this.isSecurity = this.imUser.getRole();
    this.listMember = [];

    this.isSearch = false;
    if(this.isSecurity){
      this.isEdit = false;
    } else{
      this.isEdit = true;
    }

    /*this.events.subscribe('updateScreenMember', () => {
      this.zone.run(() => {
        console.log('force update the screen');
        this.getData();
      });
    });*/

    this.params.data = {
      "title": "Tidak ada data",
      "iconNoItem": "icon-account-alert",
      "noItem": this.noMember,
      "isEdit": this.isEdit,
      "isAdd": true,
      "isSearch": this.isSearch,
      "items": this.listMember,
    };

    this.params.events = {
      'onItemClick': (item: any): void => {this.detailMember(item.id)},
      'onDelete': (item: any): void => {this.deleteMember(item)},
      'onEdit': (item: any): void => {this.presentModal(item)},
      'onFab': (item: any): void => {this.presentModal()}
    };

  }

  getData(){
    // this.util.presentLoading();
    let input = {
      email: this.emailUser
    };
    this.general.callBackend('getMemberByUser',input).subscribe((data:any) => {
        console.log(data);
        // this.util.dismissLoading();
        if(data.isSuccess=='Y'){
          this.listMember = [];
          if(data.memberList.length>0){
            for (let i=0; i<data.memberList.length;i++) {
              let memberMap:any={};
              let member = data.memberList[i];
              memberMap.title = member.name;
              memberMap.id=member.uid;
              // memberMap.description = member.relationship;
              memberMap.image = member.imageUrl;
              if(member.imageUrl==null)
                memberMap.image = member.gender=='Pria'?"assets/img/people.png":"assets/img/female.png";
              memberMap.iconDelete="icon-delete";
              memberMap.iconEdit="icon-pencil";
              memberMap.iconUndo="icon-undo-variant";
              this.listMember.push(memberMap);
            }
            this.noMember=false;
          } else {
            this.noMember=true;
          }
          this.params.data = {
            "title": "Tidak ada data",
            "iconNoItem": "icon-account-alert",
            "noItem": this.noMember,
            "isEdit": this.isEdit,
            "isAdd": true,
            "isSearch": this.isSearch,
            "items": this.listMember,
          };
        } else {
          this.util.loadAlert("Error", data.message);
        }
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

  ionViewWillEnter() {
    this.util.presentLoading();
    this.getData();
    this.util.dismissLoading();
    this.theProvider.updateMessageCount();
  }

  presentModal(item?: any) {
    let modal = this.modalCtrl.create('AddMemberPage',{memberId: item});
    modal.present();
    modal.onDidDismiss(data=>{
      console.log(data);
      if(data!=undefined && data!='N'){
        this.util.presentLoading();
        var input=data;
        input.propertyId = this.propertyId;
        input.userEmail = this.emailUser;
        let endPoint = "";
        if(item==undefined||item==null){
          endPoint = "createMember";
        } else {
          input.memberId = item;
          endPoint="updateMember";
        }
        if(data.picture.indexOf("http")<0 && data.picture.indexOf("assets")<0){
          input.imageFile = data.picture;
          input.imageFileName = data.name;
        }
        this.general.callBackend(endPoint,input).subscribe((data:any)=>{
          console.log(data);
          // this.util.dismissLoading();
          if(data.isSuccess=='Y'){
            this.noMember=false;
            /*this.util.loadAlert("Sukses", data.message);
            this.events.publish('updateScreenMember');*/
            this.getData();
            this.zone.run(() => {
              console.log('force update the screen');
              this.util.dismissLoading();
              this.util.loadAlert("Sukses", data.message);
            });
          } else {
            this.util.loadAlert("Error", data.message);
          }
        });
      }
    })
  }

  detailMember(idMember){
    let modal = this.modalCtrl.create('DetailMemberPage',{memberId: idMember});
    modal.present();
    modal.onDidDismiss(data=>{
      console.log(data);
    });
  }

  deleteMember(item){
    this.util.presentLoading();
    let input = {
      email:this.emailUser,
      memberId:item
    };
    this.general.callBackend("deleteMember",input).subscribe((data:any)=>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.noMember=false;
        this.getData();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });
  }
}
