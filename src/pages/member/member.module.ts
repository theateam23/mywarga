import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MemberPage} from './member';
import {ListItemModule} from "../../component/listingView/list-item.module";

@NgModule({
  declarations: [
    MemberPage,
  ],
  imports: [
    IonicPageModule.forChild(MemberPage),
    ListItemModule
  ],
})
export class MemberPageModule {}
