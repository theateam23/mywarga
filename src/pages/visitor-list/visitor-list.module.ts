import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {VisitorListPage} from './visitor-list';
import {ListItemModule} from "../../component/listingView/list-item.module";

@NgModule({
  declarations: [
    VisitorListPage,
  ],
  imports: [
    IonicPageModule.forChild(VisitorListPage),
    ListItemModule
  ],
})
export class VisitorListPageModule {}
