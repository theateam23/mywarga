import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {GeneralProvider} from "../../providers/general/general";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {PropertyProviders} from "../../providers/items/items";
import * as moment from "moment";
import {IdmUserServices} from "../../services/idmUserService";
import {theProvider} from "../../services/theProvider";

/**
 * Generated class for the VisitorListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visitor-list',
  templateUrl: 'visitor-list.html',
})
export class VisitorListPage {

  noVisitor:boolean;
  listVisitor:any[];
  propertyId:string;
  emailUser:string;
  params:any={};
  isSecurity:any;
  isEdit:boolean;
  isSearch:boolean;

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              public util:UtilitiesProvider,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public general:GeneralProvider,
              private property:PropertyProviders,
              public imUser:IdmUserServices,
              public theProvider:theProvider) {
    this.noVisitor = true;
    this.emailUser = this.imUser.emailUser;
    this.isSecurity = this.imUser.getRole();
    this.listVisitor = [];
    this.propertyId = this.property._propertySelected.id;

    if(this.isSecurity){
      this.isEdit = true;
      this.isSearch = true;
    } else{
      this.isEdit = false;
      this.isSearch = false;
    }
    this.params.data = {
      "title": "Tidak ada data",
      "iconNoItem": "icon-alert-octagon",
      "noItem": this.noVisitor,
      "isEdit": this.isEdit,
      "isAdd": true,
      "isSearch": this.isSearch,
      "items": this.listVisitor
    };

    this.params.events = {
      'onItemClick': function (item: any) {
        console.log(item);
      },
      'onDelete': (item: any): void => {this.updateStatus(item,"Canceled")},
      'onEdit': (item: any): void => {this.updateStatus(item,"Visited")},
      'onFab': (item: any): void => {this.presentModal()},
      'onTextChange': function(text: any) {
        console.log("onTextChange");
      }

    };
  }

  ionViewWillEnter() {
    this.util.presentLoading();
    this.getListVisitor();
    if(this.isSecurity) {
      let elms = document.querySelectorAll(".list-content");
      for(let i=0;i<elms.length;i++) {
       let elm = <HTMLElement>elms[i];
       elm.style.marginTop = "7vh";
      }
    } else {
      this.theProvider.updateMessageCount();
    }
    this.util.dismissLoading();
  }

  presentModal(item?: any) {
    let modal = this.modalCtrl.create('AddVisitorPage');
    modal.present();
    modal.onDidDismiss(data=>{
      console.log(data);
      if(data!=undefined&&data!='N'){
        this.util.presentLoading();
        var input=data;
        input.propertyId = this.propertyId;
        let timeVisit = moment(data.timeVisit).format("HH:mm");
        input.date = data.dateVisit+" "+ timeVisit;
        if(data.dateVisit==null){
          input.date = moment(new Date()).format("YYYY-MM-DD HH:mm");
        }
        input.email = this.imUser.emailUser;
        input.picture = data.base64;

        this.general.callBackend("createVisitor",input).subscribe((data:any)=>{
          // this.util.dismissLoading();
          console.log(data);
          if(data.isSuccess=='Y'){
            this.noVisitor=false;
            /*this.util.loadAlert("Sukses", data.message);
            this.events.publish('updateScreenVisitor');*/
            this.getListVisitor();
            this.zone.run(() => {
              console.log('force update the screen');
              this.util.dismissLoading();
              this.util.loadAlert("Sukses", data.message);
            });
          } else {
            this.util.loadAlert("Error", data.message);
          }
        });
      }
    });
  }

  getListVisitor(){
    // this.util.presentLoading();
    let input = {
      email: this.emailUser,
      propertyId:this.propertyId
    };
    this.general.callBackend('getVisitorList',input).subscribe((data:any) => {
        console.log(data);
        // this.util.dismissLoading();
        if(data.isSuccess=='Y'){
          this.listVisitor=[];
          if(data.visitors.length>0){
            for (let i=0; i<data.visitors.length;i++) {
              let visitor = data.visitors[i];
              let visitorMap:any={};
              visitorMap.id = visitor.recordId;
              visitorMap.title = visitor.date;
              visitorMap.description = visitor.name;
              if(this.isSecurity){
                visitorMap.subTitle = visitor.name;
                visitorMap.description = visitor.vehicleNo+"/"+visitor.phoneNo;
              }
              visitorMap.image = visitor.picture||'assets/img/people.png';
              visitorMap.iconDelete="icon-delete";
              visitorMap.iconEdit="icon-check";
              visitorMap.iconUndo="icon-undo-variant";
              this.listVisitor.push(visitorMap);
            }
            this.noVisitor=false;
          } else {
            this.noVisitor=true;
          }
          this.params.data = {
            "title": "Tidak ada data",
            "iconNoItem": "icon-alert-octagon",
            "noItem": this.noVisitor,
            "isEdit": this.isEdit,
            "isAdd": true,
            "isSearch": this.isSearch,
            "items": this.listVisitor
          };
        } else {
          this.util.loadAlert("Error", data.message);
        }
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

  updateStatus(item,status){

    var input = {
      email:this.imUser.emailUser,
      propertyId:this.propertyId,
      visitStatus:status,
      recordId:item
    };
    this.util.presentLoading();
    this.general.callBackend("updateVisitorStatus",input).subscribe((data:any) =>{
      // this.util.dismissLoading();
      if(data.isSuccess=='Y'){
        /*this.util.loadAlert("Sukses", data.message);
        this.events.publish('updateScreenVisitor');*/
        this.getListVisitor();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });
  }

}
