import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {VehiclePage} from './vehicle';
import {ListItemModule} from "../../component/listingView/list-item.module";

@NgModule({
  declarations: [
    VehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclePage),
    ListItemModule
  ],
})
export class VehiclePageModule {}
