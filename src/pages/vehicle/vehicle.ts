import {Component, NgZone} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {GeneralProvider} from "../../providers/general/general";
import {PropertyProviders} from "../../providers/items/items";
import {IdmUserServices} from "../../services/idmUserService";
import { theProvider } from '../../services/theProvider';


@IonicPage()
@Component({
  selector: 'page-vehicle',
  templateUrl: 'vehicle.html',
})
export class VehiclePage {

  noVehicle:boolean;
  listVehicle:any[];
  propertyId:string;
  emailUser:string;
  profilePic:string;
  params:any={};

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              public util:UtilitiesProvider,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public general:GeneralProvider,
              private imUser:IdmUserServices,
              private theProvider:theProvider,
              private property:PropertyProviders) {
    this.noVehicle = false;
    this.emailUser = this.imUser.emailUser;

    this.listVehicle = [];
    this.propertyId = this.property._propertySelected.id;

    this.params.data = {
      "title": "Tidak ada data",
      "iconNoItem": "icon-alert-octagon",
      "noItem": this.noVehicle,
      "isEdit": true,
      "isAdd": true,
      "isSearch": false,
      "items": this.listVehicle
    };

    this.params.events = {
      'onItemClick': (item: any): void => {this.detailVehicle(item.id)},
      'onDelete': (item: any): void => {this.deleteVehicle(item)},
      'onEdit': (item: any): void => {this.presentModal(item)},
      'onFab': (item: any): void => {this.presentModal()}
    };

  }
  ionViewDidLoad(){
    console.log("ionViewDidLoad");
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter");
    this.util.presentLoading();
    this.getListVehicle();
    this.util.dismissLoading();
    this.theProvider.updateMessageCount();
  }

  ionViewDidEnter(){
    console.log("ionViewDidEnter");
  }

  presentModal(item?: any) {
    let modal = this.modalCtrl.create('AddVehiclePage',{vehicleId: item});
    modal.present();
    modal.onDidDismiss(data=>{
      console.log(data);
      if(data!=undefined&&data!='N'){
        this.util.presentLoading();
        var input=data;
        input.propertyId = this.propertyId;
        input.userEmail = this.emailUser;
        let endPoint = "";
        if(item==undefined||item==null){
          endPoint = "createVehicle";
        } else {
          input.vehicleId = item;
          endPoint="updateVehicle";
        }
        if(data.picture.indexOf("http")<0 && data.picture.indexOf("assets")<0){
          input.imageFile = data.picture;
          input.imageFileName = data.name;
        }
        this.general.callBackend(endPoint,input).subscribe((data:any)=>{
          console.log(data);
          // this.util.dismissLoading();
          if(data.isSuccess=='Y'){
            this.noVehicle=false;
            /*this.util.loadAlert("Sukses", data.message);
            this.events.publish('updateScreenVehicle');*/
            this.getListVehicle();
            this.zone.run(() => {
              console.log('force update the screen');
              this.util.dismissLoading();
              this.util.loadAlert("Sukses", data.message);
            });
          } else {
            this.util.loadAlert("Error", data.message);
          }
        });
      }
    });
  }

  getListVehicle(){
    // this.util.presentLoading();
    let input = {
      email: this.emailUser
    };
    this.general.callBackend('getVehicleByUser',input).subscribe((data:any) => {
        console.log(data);
        if(data.isSuccess=='Y'){
          this.listVehicle=[];
          if(data.vehicleList.length>0){
            for (let i=0; i<data.vehicleList.length;i++) {
              let vehicle = data.vehicleList[i];
              let vehicleMap:any={};
              vehicleMap.title = vehicle.plateNo;
              vehicleMap.description = vehicle.vehicleType +" - "+ vehicle.brand;
              vehicleMap.id = vehicle.uid;
              vehicleMap.image = vehicle.imageUrl;
              if(vehicle.imageUrl==null){
                let vehicleType = "assets/img/car.png";
                if(vehicle.vehicleType=="Truk"){
                  vehicleType = "assets/img/truck.png";
                } else if(vehicle.vehicleType=="Sepeda"){
                  vehicleType = "assets/img/cycle.png";
                } else if(vehicle.vehicleType=="Sepeda Motor"){
                  vehicleType = "assets/img/motor.png";
                } else if(vehicle.vehicleType=="Mobil"){
                  vehicleType = "assets/img/car.png";
                }
                vehicleMap.image = vehicleType;
              }
              vehicleMap.iconDelete="icon-delete";
              vehicleMap.iconEdit="icon-pencil";
              vehicleMap.iconUndo="icon-undo-variant";
              this.listVehicle.push(vehicleMap);
            }
            this.noVehicle=false;
          } else {
            this.noVehicle=true;
          }
          this.params.data = {
            "title": "Tidak ada data",
            "iconNoItem": "icon-alert-octagon",
            "noItem": this.noVehicle,
            "isEdit": true,
            "isAdd": true,
            "isSearch": false,
            "items": this.listVehicle
          };
        } else {
          this.util.loadAlert("Error", data.message);
        }
        // this.util.dismissLoading();
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

  detailVehicle(idMember){
    let modalDetailVehicle = this.modalCtrl.create('DetailVehiclePage',{vehicleId: idMember});
    modalDetailVehicle.present();
    modalDetailVehicle.onDidDismiss(data=>{
      console.log(data);
    });
  }

  deleteVehicle(item){
    this.util.presentLoading();
    let input = {
      email:this.emailUser,
      vehicleId:item
    };
    this.general.callBackend("deleteVehicle",input).subscribe((data:any)=>{
      console.log(data);
      // this.util.dismissLoading();
      if(data.isSuccess=='Y'){
        this.noVehicle=false;
        /*this.util.loadAlert("Sukses", data.message);
        this.events.publish('updateScreenVehicle');*/
        this.getListVehicle();
        this.zone.run(() => {
          console.log('force update the screen');
          this.util.dismissLoading();
          this.util.loadAlert("Sukses", data.message);
        });
      } else {
        this.util.loadAlert("Error", data.message);
      }
    });

  }

}
