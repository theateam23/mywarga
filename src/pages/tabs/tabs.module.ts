import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabsPage} from './tabs';
import {TabsLayout2Module} from "../../component/tabIcon/tabs-layout-2.module";

@NgModule({
  declarations: [
    TabsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsPage),
    TabsLayout2Module
  ],
  /*schemas: [CUSTOM_ELEMENTS_SCHEMA],*/
  exports: [
    TabsPage
  ]
})
export class TabsPageModule { }
