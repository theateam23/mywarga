import {ChangeDetectorRef, Component} from '@angular/core';
import {Events, IonicPage, MenuController, NavController} from 'ionic-angular';

import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {NativeStorage} from "@ionic-native/native-storage";
import {User} from "../../providers/user/user";
import {IdmUserServices} from "../../services/idmUserService";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  params:any={};
  emailUser:String;
  isSecurity:any;
  messageCount:any=0;
  constructor(public events: Events, public detectorRef: ChangeDetectorRef,public navCtrl: NavController, private imUserService: IdmUserServices,
              public user:User, public nativeStorage:NativeStorage, public util:UtilitiesProvider, private menuCtrl:MenuController) {
    this.menuCtrl.enable(true,'otherMenu');
    this.isSecurity= this.imUserService.getRole();

    this.params.data = [
      { page: "DashboardPage", title: "Informasi",icon: "warga-icon-wargakita-dashboard",badge:0 }
    ];

    if(this.isSecurity==false){
      this.params.data.push(
        { page: "MemberPage", title: "Penghuni",icon: "warga-icon-wargakita-member",badge:0 },
        { page: "VehiclePage", title: "Kendaraan",icon: "warga-icon-wargakita-vehicle",badge:0 },
        { page: 'VisitorListPage', title: 'Pengunjung', icon : "warga-icon-wargakita-visitor",badge:0 }, //paguyuban tidak dpt
        { page: 'MessageListPage', title: 'Pesan', icon : "warga-icon-wargakita-inbox",badge:0 }
        );

      events.subscribe('message:updated', _badgeValue => {
        this.params.data[4].badge = _badgeValue;
        detectorRef.detectChanges();
      });

    } else {
      this.params.data.push({ page: "VisitorListPage", title:"Pengunjung",icon: "warga-icon-wargakita-visitor",badge:0 });
      this.params.data.push({ page: "SearchMemberPage", title:"Penghuni",icon: "warga-icon-wargakita-member",badge:0 });
      this.params.data.push({ page: "SearchVehiclePage", title:"Kendaraan",icon: "warga-icon-wargakita-vehicle",badge:0 });
    }

    this.params.events = {
      'onItemClick': function (item: any) {
        console.log("onItemClick");
      }
    };

  }

}
