import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IdmUserServices} from "../../services/idmUserService";

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  formUser: { uid:string, name: string, email: string,mobileNo:string,address:string,picture:string} = {
    name: '',
    email: '',
    mobileNo:'',
    address:'',
    uid:'',
    picture:''
  };
  profileForm:FormGroup;
  base64Image='assets/img/people.png';
  imageData='';
  constructor(public navCtrl: NavController, private imUser:IdmUserServices, public viewCtrl:ViewController, public params: NavParams, public camera:Camera, public formBuilder: FormBuilder, public util:UtilitiesProvider) {
    this.formUser = this.params.get("user");
    this.base64Image = this.formUser.picture;
    this.formUser.email = this.imUser.emailUser;

    this.profileForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      mobileNo: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  ionViewWillEnter(){
    this.profileForm.controls['name'].setValue(this.formUser.name);
    this.profileForm.controls['mobileNo'].setValue(this.formUser.mobileNo);
    this.profileForm.controls['email'].setValue(this.imUser.emailUser);
    this.base64Image = this.formUser.picture==""?"assets/img/people.png":this.formUser.picture;
  }

  dismiss(send) {
    if(send==='Y'){
      this.formUser.picture = this.imageData;
      this.profileForm.value.picture = this.imageData;
      this.viewCtrl.dismiss(this.profileForm.value);
    } else {
      this.viewCtrl.dismiss(send);
    }
  }

  takePicture(){
    let options =
      {
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 512,
        targetHeight: 512,
        quality: 50,
        correctOrientation: true,
        allowEdit:true
      };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.imageData = imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
