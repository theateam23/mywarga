import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SocialSharing} from "@ionic-native/social-sharing";
import {AppVersion} from "@ionic-native/app-version";

/**
 * Generated class for the AboutUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {

  version:any;
  myRow=3;
  description:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private socialSharing: SocialSharing, private appVersion: AppVersion) {
    this.appVersion.getVersionNumber().then(version=>{
      this.version = version;
    });

    this.description = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUsPage');
  }

  shareThis(){
    this.socialSharing.share("Warga Kita","Visit us",null,"www.warga-kita.com").then((data:any)=>{
      console.log(data);
    }).catch((error:any)=>{
      console.log(error);
    })
  }

}
