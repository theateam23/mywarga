import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {FormRequestPage} from './form-request';
import {SelectLayout3Module} from "../../component/droplist/select-layout-3.module";

@NgModule({
  declarations: [
    FormRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(FormRequestPage),
    SelectLayout3Module
  ],
})
export class FormRequestPageModule {}
