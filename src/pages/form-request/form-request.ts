import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {GeneralProvider} from "../../providers/general/general";
import {PropertyProviders} from "../../providers/items/items";

/**
 * Generated class for the FormRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-request',
  templateUrl: 'form-request.html',
})
export class FormRequestPage {
  description:any;
  fileNameDownload:any;
  urlDownload:any;
  params:any={};
  listForm:any=[];
  selectedId:any;
  myRow=8;
  constructor(public navCtrl: NavController, public navParams: NavParams,private general:GeneralProvider, private property:PropertyProviders) {
  }

  ionViewDidEnter(){
    this.general.callBackend("getRequestFormList",{propertyId:this.property._propertySelected.id}).subscribe((response:any) => {
      if(response.isSuccess=='Y') {
        this.listForm = response.formList;
        this.params.data = {
          "title" : "Daftar Formulir",
          "header" : "Formulir",
          "selectedItem": 99,
          "items" : this.listForm
        };
      } else {
        console.error(response.message);
      }
    });


    this.params.events = {
      'onSelect': (item): void => {this.changeDroplist(item)},
    };
  }

  displayDesc(){
    let filterData = this.listForm.find(x => x.id == this.selectedId);
    this.urlDownload = filterData.fileUrl;
    this.fileNameDownload = filterData.fileName;
    this.description = filterData.description;
  }

  changeDroplist(item){
    this.description="";
    this.fileNameDownload="";
    this.urlDownload="";
    this.selectedId = item;
  }

}
