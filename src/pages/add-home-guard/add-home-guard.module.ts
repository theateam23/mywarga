import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AddHomeGuardPage} from './add-home-guard';

@NgModule({
  declarations: [
    AddHomeGuardPage,
  ],
  imports: [
    IonicPageModule.forChild(AddHomeGuardPage),
  ],
})
export class AddHomeGuardPageModule {}
