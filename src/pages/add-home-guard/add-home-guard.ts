import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";

/**
 * Generated class for the AddHomeGuardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-home-guard',
  templateUrl: 'add-home-guard.html',
})
export class AddHomeGuardPage {

  jagainForm:FormGroup;
  title:any;
  minDate:any;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder,
              public navParams: NavParams,public viewCtrl: ViewController) {

    this.jagainForm = formBuilder.group({
      fromDate: ['', Validators.compose([Validators.required])],
      toDate: ['', Validators.compose([Validators.required])],
      description: [''],
    },{ validator: Validators.compose([
      DateValidators.dateLessThan('fromDate', 'toDate', { 'rangeError': true }),
    ])});


    this.minDate = new Date().toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddHomeGuardPage');
  }


  dismiss(send) {
    if(send==='Y'){
      if(this.jagainForm.valid){
        this.viewCtrl.dismiss(this.jagainForm.value);
      }
    } else {
      this.viewCtrl.dismiss(send);
    }
  }

}


export class DateValidators {
  static dateLessThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      const date1 = c.get(dateField1).value;
      const date2 = c.get(dateField2).value;
      if ((date1 !== null && date2 !== null) && date1 > date2) {
        return validatorField;
      }
      return null;
    };
  }
}
