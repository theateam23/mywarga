import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {SMS} from "@ionic-native/sms";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {PropertyProviders} from "../../providers/items/items";
import {CallNumber} from "@ionic-native/call-number";

/**
 * Generated class for the PanicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-panic',
  templateUrl: 'panic.html',
})
export class PanicPage {

  listSecurity:any;
  phoneNo:any;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private call:CallNumber, public navParams: NavParams,
              private sms:SMS, private util:UtilitiesProvider, private property:PropertyProviders) {
    this.listSecurity = this.property._propertySelected.securityList;
    // this.listSecurity = [{name:"himawan",phoneNo:"083831888877"},{name:"arif",phoneNo:"083831888877"},{name:"rachman",phoneNo:"081381182312"},{name:"himawan",phoneNo:"081381182312"}]
  }

  ionViewDidLoad() {
    this.clearBgColor();
    this.changeSecurity(this.listSecurity[0]);
    console.log('ionViewDidLoad PanicPage');
  }
  clearBgColor(){
    this.listSecurity.forEach((item) => {
      item.color = "transparent";
    });
  }

  changeSecurity(security){
    this.phoneNo = security.phoneNo;
    this.clearBgColor();
    if(security.color==="#1CA2B3"){
      security.color = "transparent";
    } else {
      security.color = "#1CA2B3";
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  callSecurity() {
    this.call.callNumber(this.phoneNo, true).then(function (data) {
      console.log(data);
    });

  }

  async sendSms(){
    try{
      let unit = this.property._propertySelected.unitName;
      await this.sms.send(this.phoneNo,'Penghuni unit '+unit+' butuh bantuan anda, segera ke sana.');
      this.util.loadAlert("Thank You", "Security akan segera datang.");
    } catch (e) {
      console.error(e);
      // this.util.loadAlert("Sorry", "Ada masalah, silahkan kontak admin");
    }
  }

}
