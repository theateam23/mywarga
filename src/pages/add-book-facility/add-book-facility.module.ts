import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AddBookFacilityPage} from './add-book-facility';

@NgModule({
  declarations: [
    AddBookFacilityPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBookFacilityPage),
  ],
})
export class AddBookFacilityPageModule {}
