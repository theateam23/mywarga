import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import * as moment from "moment";
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-add-book-facility',
  templateUrl: 'add-book-facility.html',
})
export class AddBookFacilityPage {

  booking: any  = { startTime: new Date(), endTime: new Date(), allDay: false };
  facility:any;
  selectedDate:any;
  minuteValues=[];
  hourValues=[];
  bookingForm:FormGroup;
  listBooking=[];
  myRow=4;

  constructor(public navCtrl: NavController,public formBuilder: FormBuilder, public navParams: NavParams, public viewCtrl: ViewController) {
    this.minuteValues = ['0'];
    this.hourValues = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'];
    this.facility = this.navParams.get('facility');
    this.booking.selectedDate = moment(this.navParams.get('selectedDay')).format('YYYY-MM-DD');
    this.booking.facilityId = this.facility?this.facility.id:'';
    let now = moment('00:00','HH:mm');
    this.bookingForm = formBuilder.group({
      startTime: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      endTime: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      cbTnc: [false,Validators.compose([CheckboxValidator.isChecked, Validators.required])],
      tnc: [''],
    },{ validator: Validators.compose([
        DateValidators.dateLessThan('startTime', 'endTime', { 'rangeError': true }),
      ])});
    this.listBooking = this.navParams.get('listBooking');
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.viewCtrl.dismiss(this.bookingForm.value);
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter AddBookFacilityPage');
    this.bookingForm.controls['tnc'].setValue(this.facility.tnc);
  }

}

export class CheckboxValidator{

  static isChecked(control: FormControl) : any{
    if(control.value != true){
      return {
        "notChecked" : true
      };
    }

    return null;
  }

}

export class DateValidators {
  static dateLessThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      const date1 = c.get(dateField1).value;
      const date2 = c.get(dateField2).value;
      if ((date1 !== null && date2 !== null) && date1 > date2) {
        return validatorField;
      }
      return null;
    };
  }
}
