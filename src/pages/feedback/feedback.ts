import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {GeneralProvider} from "../../providers/general/general";
import {PropertyProviders} from "../../providers/items/items";
import * as moment from "moment";
import {MainPage} from "../pages";
import {IdmUserServices} from "../../services/idmUserService";

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  data:any={};
  base64Image:any="";
  title:any;
  description:any;
  strbase64:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public camera:Camera,
              public util:UtilitiesProvider, private general:GeneralProvider,private imUser:IdmUserServices,
              private property:PropertyProviders) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  takePicture(){
    let options = {
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 512,
        targetHeight: 512,
        quality:75,
        correctOrientation: true,
        allowEdit:true
      };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  openGallery(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality:75,
      targetWidth: 512,
      targetHeight: 512,
      correctOrientation: true,
      // allowEdit:true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.strbase64 = imageData;
      // console.log(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  takeOrUploadPicture(){
    let dataInput= {
      title : "Please Choose",
      listBtn : [
        {
          text: "Open Gallery",
          handler:(): void => {this.openGallery()}
        },
        {
          text: "Take Picture",
          handler:(): void => {this.takePicture()}
        }
      ]
    };
    this.util.openActionSheet(dataInput);
  }

  submit(){

    let input = {
      propertyId:this.property._propertySelected.id,
      email:this.imUser.emailUser,
      title:this.title,
      description:this.description,
      attachment:this.strbase64,
      fileName:moment(new Date()).format("YYYY-MM-DD_HH:mm:ss")
    };

    this.general.callBackend("submitFeedback",input).subscribe((response:any) => {
      if(response.isSuccess=='Y') {
        console.log(response.message);
        this.util.loadAlert("Sukses",response.message);
        this.navCtrl.push(MainPage);
      } else {
        console.error(response.message);
        this.util.loadAlert("Error",response.message);
      }
    });
  }

  clear(){
    this.title="";
    this.description="";
    this.base64Image="";
  }

}
