import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {GeneralProvider} from "../../providers/general/general";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {IdmUserServices} from "../../services/idmUserService";

/**
 * Generated class for the SearchVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-vehicle',
  templateUrl: 'search-vehicle.html',
})
export class SearchVehiclePage {
  params:any={};

  constructor(public navCtrl: NavController, private imUser:IdmUserServices, public navParams: NavParams, private general:GeneralProvider, private util:UtilitiesProvider) {
    this.params.data = {
      "isFound": false,
      "placeHolder":"Masukkan plat no"
    };

    this.params.events = {
      'onSearch': (item: any): void => {this.searchData(item)}
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchVehiclePage');
  }

  searchData(item){
    this.util.presentLoading();
    console.log("item---> "+item);
    let input={
      name:item,
      email:this.imUser.emailUser
    };
    this.general.callBackend('getVehicleByPlateNo',input).subscribe((data:any) => {
        console.log(data);
        if(data.isSuccess=='Y'){
          this.params.data = {
            "isFound": true
          };
        } else {
          this.params.data = {
            "isFound": false
          };
        }
        this.util.dismissLoading();
      },
      e => {
        this.util.dismissLoading();
        console.log(e);
        this.util.loadToast(e);
      }
    );
  }

}
