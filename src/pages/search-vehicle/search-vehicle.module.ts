import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchVehiclePage} from './search-vehicle';
import {SearchBarRemoteModule} from "../../component/search-remote/search-bar-remote.module";

@NgModule({
  declarations: [
    SearchVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchVehiclePage),
    SearchBarRemoteModule
  ],
})
export class SearchVehiclePageModule {}
