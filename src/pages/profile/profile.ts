import {Component, NgZone, ViewChild} from '@angular/core';
import {AlertController, App, Events, IonicPage, ModalController, Nav, NavController, NavParams} from 'ionic-angular';
import {User} from "../../providers/user/user";
import {IdmUserServices} from "../../services/idmUserService";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {Camera} from "@ionic-native/camera";
import * as firebase from "firebase/app";
import {NativeStorage} from "@ionic-native/native-storage";
import {Firebase} from "@ionic-native/firebase";
import {PropertyProviders} from "../../providers/items/items";
import {TranslateService} from "@ngx-translate/core";
import {GeneralProvider} from "../../providers/general/general";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild(Nav) nav: Nav;
  user: any;
  base64Image='assets/img/people.png';
  imageData='';
  lang:any;
  isSecurity:any;
  constructor(public translate: TranslateService,public navCtrl: NavController,public alertCtrl:AlertController,
              public property: PropertyProviders,public fBase:Firebase, public app:App,
              private nativeStorage: NativeStorage, public events:Events,private zone: NgZone, public modalCtrl:ModalController,
              public camera:Camera, public util:UtilitiesProvider, public navParams: NavParams, public userService:User,
              public imUser:IdmUserServices, private general:GeneralProvider) {
    this.user = {};
    this.isSecurity = this.imUser.getRole();
    this.events.subscribe('updateScreenProfile', () => {
      this.zone.run(() => {
        console.log('force update the screen');

        this.getDetailUser();
      });
    });

    this.nativeStorage.getItem('lang')
      .then(
        dataStorage => {
          console.log(dataStorage);
          this.lang = dataStorage;
        },
        error => {
          console.error(error);
          this.lang = "id";
          this.translate.setDefaultLang("id");
        }
      );

  }

  switchLanguage() {
    this.translate.use(this.lang);
    this.nativeStorage.setItem("lang",this.lang);
  }

  ionViewDidEnter() {
    this.getDetailUser();
  }

  getDetailUser(){
    let email = this.imUser.emailUser;
    // this.userService.getUser({email:email}).subscribe((resp:any) => {
    this.general.callBackend("getUser",{email:email}).subscribe((resp:any) => {
        // console.log("get user --->"+JSON.stringify(resp));
        if(resp.isSuccess =='Y'){
          this.user = resp.user;
          this.user.email = email;
          this.user.unitName = this.property._propertySelected.unitName;
          this.user.areaName = this.property._propertySelected.areaName;
          this.user.propertyName = this.property._propertySelected.name;
          if(resp.user.imageUrl!=null || resp.user.imageUrl != undefined)
            this.base64Image = resp.user.imageUrl;
          else
            this.base64Image='assets/img/people.png';
        }

      },e => {
        this.util.loadToast(JSON.stringify(e));
        console.log(e)
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  presentModal(page){
    this.user.picture = this.base64Image;
    let modal = this.modalCtrl.create(page, {user: this.user});
    modal.present();
    modal.onDidDismiss(data => {
      if(data!="N"){
        this.user = data;
        if(data.picture==""){
          this.user.picture =  null;
        } else {
          this.imUser.setPictureProfile(this.user.picture);
        }
        // this.userService.updateUser(this.user).subscribe((resp:any) => {
        this.general.callBackend("updateUser",this.user).subscribe((resp:any) => {
            if(resp.isSuccess =='Y'){
              this.util.loadAlert("Sukses", "Data profil anda sudah diperbaharui");
              this.events.publish('updateScreenProfile');
            }

          },e => {
            this.util.loadToast(JSON.stringify(e));
            console.log(e)
          }
        );

      }
    });
  }

  doLogout() {
    let alert = this.alertCtrl.create({
      title: 'Sign Out',
      subTitle: 'Apakah anda ingin keluar?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('cancel log out');
        }
      },
        {
          text:'OK',
          handler: data => {
            if(this.imUser.getRole()){
              this.fBase.unsubscribe("security~"+this.property._propertySelected.id);
            }
            this.fBase.unsubscribe(this.property._propertySelected.id);
            this.fBase.unsubscribe(this.user.id);
            this.fBase.unregister();
            firebase.auth().signOut();
            this.imUser.setLoginFlag(false);
            this.app.getRootNav().setRoot('WelcomePage');
            this.nativeStorage.remove('propertySelected');
            this.nativeStorage.remove('isLogin');
          }
        }
      ]
    });
    alert.present();
  }
}
