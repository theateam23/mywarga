export const LIST_VEHICLE_TYPE = [
  {
    id: "Sepeda",
    name: "Sepeda"
  },
  {
    id: "Sepeda Motor",
    name: "Sepeda Motor"
  },
  {
    id: "Mobil",
    name: "Mobil"
  },
  {
    id: "Truk",
    name: "Truk"
  },
  {
    id: "Lainnya",
    name: "Lainnya"
  }
];

export const LIST_GENDER = [
  {
    id: "Pria",
    name: "Pria"
  },
  {
    id: "Wanita",
    name: "Wanita"
  }
];

export const LIST_RELATION = [
  {
    id: "Suami",
    name: "Suami"
  },
  {
    id: "Istri",
    name: "Istri"
  },
  {
    id: "Anak",
    name: "Anak"
  },
  {
    id: "Orang Tua",
    name: "Orang Tua"
  },
  {
    id: "ART",
    name: "Asisten Rumah Tangga"
  },
  {
    id: "Lainnya",
    name: "Lainnya"
  }
];

export const LIST_STATUS_MEMBER = [
  {
    id: "Pemilik",
    name: "Pemilik"
  },
  {
    id: "Penyewa",
    name: "Penyewa"
  },
  {
    id: "Penghuni",
    name: "Penghuni"
  }
];
