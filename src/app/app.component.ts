import {Component, ViewChild} from '@angular/core';
import {App, Events, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {FirstRunPage, MainPage} from "../pages/pages";
import {Keyboard} from "@ionic-native/keyboard";
import {IdmUserServices} from "../services/idmUserService";
import {TranslateService} from "@ngx-translate/core";
import {NativeStorage} from "@ionic-native/native-storage";
import * as firebase from "firebase/app";
import {environment} from "../environment/environment";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage = FirstRunPage;
  rootPage;
  @ViewChild(Nav) nav: Nav;
  params:any={};
  isSecurity:any;
  pages: any[];
  bgImg:any="assets/imgs/16.jpg";
  account:any={};

  constructor(public events: Events,private nativeStorage: NativeStorage,private translate: TranslateService,
              public platform: Platform, private statusBar: StatusBar,
              public imUser:IdmUserServices, private splashScreen: SplashScreen, public  app: App,
               private keyboard:Keyboard) {
    firebase.initializeApp(environment.firebase);
    this.events.subscribe("UPDATE_IMG_SIDE_MENU", (imgUrl) => {
      this.bgImg = imgUrl;
    });

    this.events.subscribe("UPDATE_SIDE_MENU", (paramSideMenu) => {
      //Logic to update Side-menu
      // console.log(isSecurity);

      if(paramSideMenu.isSecurity=="Y"){
        this.pages = [
          { title: 'Profile', component: 'ProfilePage',"icon" : "warga-icon-wargakita-user-profile" },
          { title: 'Pesan', component: 'MessageListPage',  "icon" : "warga-icon-wargakita-inbox" },
          { title: 'Tentang Kita', component: 'AboutUsPage',  "icon" : "warga-icon-wargakita-info" },
        ];
      } else {
        this.pages = [];
        this.pages.push({title: 'Profile', component: 'ProfilePage', "icon": "warga-icon-wargakita-user-profile"});
        this.pages.push({ component: "HomeGuardListPage", title:"Jaga Rumah",icon: "warga-icon-wargakita-guard-home" });
        this.pages.push({ component: "FormRequestPage", title: "Formulir",icon: "warga-icon-wargakita-request-form"  });
        if (paramSideMenu.type == "Apartemen") {
          this.pages.push({ title: 'Pesan Fasilitas', component: 'FacilityBookingPage',  "icon" : "warga-icon-wargakita-booking" });
        }
        this.pages.push({title: 'Kritik dan Saran', component: 'FeedbackPage', "icon": "warga-icon-wargakita-feedback"});
        this.pages.push({title: 'Tentang Kita', component: 'AboutUsPage', icon: "warga-icon-wargakita-info"});
      }

    });


    platform.ready().then(() => {
      this.isSecurity = this.imUser.getRole();
      this.nativeStorage.getItem('isLogin')
        .then(
          dataStorage => {
            console.log(dataStorage);
            if(dataStorage) {
              this.nativeStorage.getItem('emailUser')
                .then(email => {
                    console.log(email);
                    this.imUser.emailUser = email;
                    if (dataStorage)
                      this.rootPage = MainPage;
                    else
                      this.rootPage = FirstRunPage;
                  },
                  error => {
                    console.error(error);
                    this.rootPage = FirstRunPage;
                  }
                );
            } else
              this.rootPage = FirstRunPage;
          },
          error => {
            console.error(error);
            this.rootPage = FirstRunPage;
          }
        );
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();
      platform.registerBackButtonAction(() => {
        console.log("You can't go back");
      }, 0);
    });
    this.initTranslate();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('id');
    this.nativeStorage.setItem("lang",'id')
  }

  initializeApp() {
    this.keyboard.disableScroll(false);
  }

  openPage(page){
    this.nav.push(page.component);
  }

  sideMenuOpen() {
    console.log("sideOpen");
  }
}

