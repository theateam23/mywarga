import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ErrorHandler, NgModule} from "@angular/core";
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from "@angular/platform-browser";
import {Camera} from "@ionic-native/camera";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {IonicStorageModule} from "@ionic/storage";
import {IonicApp, IonicModule} from "ionic-angular";

import {MyApp} from "./app.component";
import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {environment} from "../environment/environment";
import {Facebook} from "@ionic-native/facebook";
import {Api} from "../providers/api/api";
import {User} from "../providers/user/user";
import {IdmUserServices} from "../services/idmUserService";
import {PropertyProviders} from "../providers/items/items";
import {UtilitiesProvider} from "../providers/utilities/utilities";
import {NativeStorage} from "@ionic-native/native-storage";
import {VisitorProvider} from "../providers/visitor/visitor";
import {Keyboard} from "@ionic-native/keyboard";
import {NgCalendarModule} from "ionic2-calendar";
import {FacilityProvider} from '../providers/facility/facility';
import {Firebase} from "@ionic-native/firebase";
import {GeneralProvider} from '../providers/general/general';
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {AppVersion} from "@ionic-native/app-version";
import {SocialSharing} from "@ionic-native/social-sharing";
import {SMS} from "@ionic-native/sms";
import {CallNumber} from "@ionic-native/call-number";
import {MyErrorHandlerProvider} from "../providers/my-error-handler/my-error-handler";
import {LoggerService} from "../services/LoggerService";
import {SlackService} from "../services/slack.service";
import {theProvider} from "../services/theProvider";


export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    'press': { time: 2500 }  //set press delay for 1 second
  }
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
      iconMode: 'ios',
      mode:'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      pageTransition: 'ios',
      scrollAssist: false,
      autoFocusAssist: false,
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    NgCalendarModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Api,
    User,
    Camera,
    Keyboard,
    SplashScreen,
    StatusBar,
    // { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: ErrorHandler, useClass: MyErrorHandlerProvider },
    Facebook,
    IdmUserServices,
    PropertyProviders,
    UtilitiesProvider,
    { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig },
    NativeStorage,
    VisitorProvider,
    FacilityProvider,
    Firebase,
    GeneralProvider,
    AppVersion,
    SocialSharing,
    SMS,
    CallNumber,
    SlackService,
    LoggerService,
    theProvider
  ]
})
export class AppModule { }
