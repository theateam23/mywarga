import {Injectable} from '@angular/core';
import {Config} from '../config';
import {Observable} from 'rxjs';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class SlackService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  public post(message, color): Observable<any> {
    let payload = {
      attachments: [
        {
          color: color,
          text: message
        }
      ]
    };
    return this.http.post(Config.slackIncomingWebHookUrl, JSON.stringify(payload));
  }
}
