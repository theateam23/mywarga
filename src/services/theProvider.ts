import {Events} from 'ionic-angular';
import {Injectable} from "@angular/core";
import {GeneralProvider} from "../providers/general/general";
import {IdmUserServices} from "./idmUserService";

@Injectable()
export class theProvider{

  emailUser:string;

  constructor( public events: Events,private imUser:IdmUserServices,public general:GeneralProvider ) {
    this.emailUser = this.imUser.emailUser;
  }

  updateMessageCount() {
    console.log("masuk the provider");

    let input = {
      email: this.emailUser
    };
    this.general.callBackend('getMessagesByUser',input).subscribe((data:any) => {
        console.log(data);
        // this.util.dismissLoading();
        let countMessage = 0;
        if(data.isSuccess=='Y'){
          if(data.messages.length>0) {
            for (let i = 0; i < data.messages.length; i++) {
              if(data.messages[i].isRead=='N'){
                countMessage++;
              }
            }
          }
          this.events.publish('message:updated', countMessage);
        } else {
          console.log(data.message);
        }
      },
      e => {

        console.log(e);

      }
    );
  }

}
