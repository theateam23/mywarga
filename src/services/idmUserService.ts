import {Injectable} from '@angular/core';

@Injectable()
export class IdmUserServices {

  uid: string;
  providerData: any;
  role:any;
  pictureProfile:any;
  authFlag:any;
  emailUser:any="";
  uName:string="";
  loginFlag:boolean;

  constructor() {
    this.uid = '';
    this.providerData = {
      "uid": "",
      "displayName": null,
      "photoURL": null,
      "email": "",
      "phoneNumber": null,
      "providerId": ""
    };
    this.role='';
    this.pictureProfile = '';
  }

  setLoginFlag(loginFlag){
    this.loginFlag = loginFlag;
  }

  getLoginFlag(){
    return this.loginFlag;
  }

  setUName(uname){
    this.uName = uname
  }

  setIdmUser(uid, providerData){
    this.uid = uid;
    this.providerData = providerData;
  }

  setRole(role){
    this.role = role;
  }

  setPictureProfile(pic){
    this.pictureProfile = pic;
  }

  getPictureProfile(){
    return this.pictureProfile;
  }

  getRole(){
    return this.role;
  }

  getEmail(){
    return this.emailUser;
  }

  setEmail(email:any){
    this.emailUser= email;
  }


}
